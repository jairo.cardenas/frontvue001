const  app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola mundo con Vue',
        frutas: [
            {nombre: 'Pera', cantidad: 12},
            {nombre: 'Manzana', cantidad: 10},
            {nombre: 'Fresa', cantidad: 0}
        ], nuevaFruta: '',
        total: 0
    },
    methods:{
        agregarFruta(){
            this.frutas.push({
                nombre: this.nuevaFruta,
                cantidad: 0
            });
            this.nuevaFruta='';
        },
        otroMetodo(){
        }
    },
    computed:{
        sumarFrutas(){

            this.total = 0;
            for (fruta of this.frutas){
                this.total += Number(fruta.cantidad);
            }
            return this.total;
        }
    }

})